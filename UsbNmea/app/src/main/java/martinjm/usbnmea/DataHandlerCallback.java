package martinjm.usbnmea;

import android.content.Intent;

public interface DataHandlerCallback {

    void sendBroadcast(Intent intent);

    void handleError(DataHandler.ErrorCode dataHandlerErrorCode, Intent intent);
}
