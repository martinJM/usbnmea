package martinjm.usbnmea;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

// TODO: possibly clear last message when baudrate is changed

public class UsbConnection extends Service {
    /*
     * Static section
     */
    static final class REQUESTS {
        /*
         * Expects the following extras:
         *  - int "baudrate"
         */
        static final String START_USB = "martinjm.usbnmea.usb.request.start_usb";

        /*
         * Expects no extras
         */
        static final String STOP_USB = "martinjm.usbnmea.usb.request.stop_usb";
    }

    static final class REPLIES {
        /*
         * Contains no extras
         */
        static final String STATUS_CHANGE = "martinjm.usbnmea.usb.reply.status_change";

        /*
         * Contains the following extras:
         *  - errorMessage "error"
         */
         static final String ERROR = "martinjm.usbnmea.usb.reply.error";
    }

    public enum UsbStatus {
        Connected,
        Disconnected,
        WaitingOnDevice
    }

    enum StatusChange {
        UsbStarted,
        UsbStopped,
        UsbConnected,
        UsbDisconnected
    }

    static abstract class Message implements Serializable {
        private final long timestamp;

        Message() {
            this.timestamp = Calendar.getInstance().getTimeInMillis();
        }

        long getTimestamp() {
            return timestamp;
        }

        Class getType() {
            return this.getClass();
        }
    }

    private final ArrayList<Message> messages = new ArrayList<>();

    static class StatusChangeMessage extends Message {
        private final StatusChange statusChange;

        StatusChangeMessage(StatusChange sc) {
            this.statusChange = sc;
        }

        StatusChange getStatusChange() {
            return statusChange;
        }
    }

    private static final String usbPermission = "martinjm.usbnmea.usb.USB_PERMISSION";

    /*
     * Variable section
     */
    private int baudrate = 115200;
    private UsbStatus usbStatus = UsbStatus.Disconnected;

    private UsbManager usbManager;
    private UsbDeviceConnection usbDeviceConnection;
    private UsbSerialDevice usbSerialDevice;

    private boolean dataHandlerPaused = false;

    private byte[] lastMessage = {0};

    /*
     * Error section
     */
    enum ErrorCode {
        LogError,
        DataHandlerNotSet,
        // DataHandlerNull,
        ActionNull,
        NoUsbDevicesConnected,
        NoSupportedUsbDevicesConnected,
        UsbPermissionDenied,
        UsbAlreadyAttached,
        NewDeviceNotSupported
    }

    static class ErrorMessage extends Message {
        private final ErrorCode errorCode;
        private DataHandler.ErrorCode logErrorCode;

        ErrorMessage(ErrorCode errorCode) {
            this.errorCode = errorCode;
        }

        ErrorMessage(ErrorCode errorCode, DataHandler.ErrorCode logErrorCode) {
            this.errorCode = errorCode;
            this.logErrorCode = logErrorCode;
        }

        ErrorCode getErrorCode() {
            return errorCode;
        }

        @SuppressWarnings("unused")
        DataHandler.ErrorCode getLogErrorCode() {
            return logErrorCode;
        }
    }

    /*
     * Binding support
     */
    class UsbConnectionBinder extends Binder {
        UsbConnection getService() { return UsbConnection.this; }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("usbNmeaDebugging", "onBind");

        return new UsbConnectionBinder();
    }

    /*
     * DataHandler section
     */
    private DataHandler dataHandler = null;

    private final DataHandlerCallback dataHandlerCallback = new DataHandlerCallback() {
        @Override
        public void sendBroadcast(Intent intent) {
            UsbConnection.super.sendBroadcast(intent);
        }

        @Override
        public void handleError(DataHandler.ErrorCode dataHandlerErrorCode, Intent intent) {
            ErrorMessage em = new ErrorMessage(ErrorCode.LogError, dataHandlerErrorCode);
            messages.add(em);
            sendBroadcast(intent);
        }
    };

    /*
     * Helper section
     */
    private void handleStatusChange(StatusChange sc) {
        Log.d("usbNmeaDebugging", "Handle status change");

        StatusChangeMessage scm = new StatusChangeMessage(sc);
        messages.add(scm);
        sendBroadcast(
                new Intent(REPLIES.STATUS_CHANGE)
        );
    }

    private void handleError(ErrorMessage em) {
        Log.d("usbNmeaDebugging", "Handle error: " + em.errorCode);

        messages.add(em);
        sendBroadcast(new Intent(REPLIES.ERROR).putExtra("error", em));
    }

    private void handleError(ErrorCode ec) {
        handleError(new ErrorMessage(ec));
    }

    private void openDevice(UsbDevice usbDevice) {
        if (usbSerialDevice != null && usbSerialDevice.isOpen()) {
            handleError(ErrorCode.UsbAlreadyAttached);
            return;
        }

        usbDeviceConnection = usbManager.openDevice(usbDevice);
        usbSerialDevice = UsbSerialDevice.createUsbSerialDevice(usbDevice, usbDeviceConnection);

        usbSerialDevice.open();
        usbSerialDevice.setBaudRate(baudrate);
        usbSerialDevice.setDataBits(UsbSerialInterface.DATA_BITS_8);
        usbSerialDevice.setParity(UsbSerialInterface.PARITY_ODD);
        usbSerialDevice.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);

        usbSerialDevice.read(usbReadCallback);

        // Let the callee send the intent, to better match when the device is attached
        usbStatus = UsbStatus.Connected;
    }

    /*
     * Managing section
     */
    @Override
    public void onCreate() {
        Log.d("usbNmeaDebugging", "Creating UsbConnection");

        super.onCreate();

        usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

        IntentFilter filter = new IntentFilter();
        filter.addAction(usbPermission);
        registerReceiver(permissionReceiver, filter);

        filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(usbStatusReceiver, filter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if (action == null) {
            handleError(ErrorCode.ActionNull);
        } else {
            Log.d("usbNmeaDebugging", "onStartCommand: " + action);

            switch (action) {
                case REQUESTS.START_USB:
                    usbStatus = UsbStatus.WaitingOnDevice;
                    this.baudrate = intent.getIntExtra("baudrate", 115200);

                    HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();
                    if (usbDevices.isEmpty()) {
                        // We notify that there are no usb devices connected,
                        // but we leave the service going to catch newly attached
                        // devices
                        handleError(ErrorCode.NoUsbDevicesConnected);
                    } else {
                        boolean deviceFound = false;
                        for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) {
                            UsbDevice usbDevice = entry.getValue();
                            if (UsbSerialDevice.isSupported(usbDevice)) {
                                deviceFound = true;
                                if (usbManager.hasPermission(usbDevice)) {
                                    openDevice(usbDevice);
                                } else {
                                    PendingIntent pendingIntent = PendingIntent.getBroadcast(
                                            getApplicationContext(),
                                            0,
                                            new Intent(usbPermission),
                                            0
                                    );
                                    usbManager.requestPermission(usbDevice, pendingIntent);
                                }
                            }
                        }

                        if (!deviceFound) {
                            handleError(ErrorCode.NoSupportedUsbDevicesConnected);
                        }
                    }

                    handleStatusChange(StatusChange.UsbStarted);
                    break;
                case REQUESTS.STOP_USB:
                    if (usbStatus == UsbStatus.Connected) {
                        usbSerialDevice.close();
                        usbDeviceConnection.close();
                    }
                    if (dataHandler != null)
                        dataHandler.close();
                    usbStatus = UsbStatus.Disconnected;
                    handleStatusChange(StatusChange.UsbStopped);
                    stopSelf();
                    break;
                default:
                    if (dataHandler != null)
                        dataHandler.handleIntent(intent);
                    break;
            }
        }
        return START_REDELIVER_INTENT;
    }

    private final UsbSerialInterface.UsbReadCallback usbReadCallback = data -> {
        lastMessage = data;

        if (dataHandler != null)
            dataHandler.handleData(data);
//        else
//            Log.d("usbNmeaDebugging", "DataHandlerNull");
            // handleError(ErrorCode.DataHandlerNull);
            // TODO: some error message?
            //       Cannot be the handleError, as that will spam too many errors
    };

    @Override
    public void onDestroy() {
        Log.d("usbNmeaDebugging", "onDestroy");

        if (usbStatus == UsbStatus.Connected || usbStatus == UsbStatus.WaitingOnDevice) {
            if (usbStatus == UsbStatus.Connected) {
                usbSerialDevice.close();
                usbDeviceConnection.close();
            }
            handleStatusChange(StatusChange.UsbStopped);
            usbStatus = UsbStatus.Disconnected;
        }

        if (dataHandler != null) {
            dataHandler.close();
        }

        unregisterReceiver(usbStatusReceiver);
        unregisterReceiver(permissionReceiver);

        super.onDestroy();
    }

    /*
     * Settings sections
     */
    public UsbStatus getUsbStatus() {
        return this.usbStatus;
    }

    public Class<? extends DataHandler> getDataHandler() {
        if (dataHandler == null)
            return null;
        return dataHandler.getClass();
    }

    public void changeDataHandlerStatus(Object status) {
        if (dataHandler != null)
            dataHandler.changeStatus(status);
    }

    public Object getDataHandlerStatus() {
        if (dataHandler == null)
                return null;
        return dataHandler.getStatus();
    }

    public Object getDataHandlerData() {
        if (dataHandler == null)
            return null;
        return dataHandler.getData();
    }

    public void setDataHandler(Class <? extends DataHandler> dataHandlerClass) {
        if (dataHandlerClass == null) {
            dataHandler = null;
            return;
        }

        try {
            DataHandler new_dataHandler;
            new_dataHandler = dataHandlerClass.newInstance();
            new_dataHandler.create(getApplicationContext(), dataHandlerCallback);
            dataHandler = new_dataHandler;
            Log.d("usbNmeaDebugging", "DataHandler: " + dataHandlerClass);
        } catch (IllegalAccessException | InstantiationException e) {
            handleError(ErrorCode.DataHandlerNotSet);
        }
    }

    public int getBaudrate() {
        return this.baudrate;
    }

    public void setBaudrate(int baudrate) {
        this.baudrate = baudrate;

        if (usbStatus == UsbStatus.Connected)
            usbSerialDevice.setBaudRate(this.baudrate);
    }

    public ArrayList<Message> getMessages() {
        Log.d("usbNmeaDebugging", "Messages size: " + messages.size());

        @SuppressWarnings("unchecked")
        final ArrayList<Message> clone = (ArrayList<Message>) messages.clone();
        return clone;
    }

    public void clearMessages() {
        messages.clear();
    }

    public byte[] getLastMessage() {
        return lastMessage.clone();
    }

    /*
     * Broadcast receiver section
     */
    private final BroadcastReceiver permissionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null)
                return;
            if (action.equals(usbPermission)) {
                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                    UsbDevice usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (usbDevice != null) {
                        openDevice(usbDevice);
                        if (dataHandlerPaused && dataHandler != null) {
                            dataHandler.resume();
                            dataHandlerPaused = false;
                        }
                        handleStatusChange(StatusChange.UsbConnected);
                    }
                } else {
                    handleError(ErrorCode.UsbPermissionDenied);
                }
            }
        }
    };

    private final BroadcastReceiver usbStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null)
                return;
            if (action.equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
                if (usbStatus != UsbStatus.WaitingOnDevice)
                    return;

                UsbDevice attachedDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (attachedDevice == null)
                    return;
                if (UsbSerialDevice.isSupported(attachedDevice)) {
                    if (usbManager.hasPermission(attachedDevice)) {
                        openDevice(attachedDevice);
                        if (dataHandlerPaused && dataHandler != null) {
                            dataHandler.resume();
                            dataHandlerPaused = false;
                        }
                        handleStatusChange(StatusChange.UsbConnected);
                    } else {
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                                getApplicationContext(),
                                0,
                                new Intent(usbPermission),
                                0
                        );
                        usbManager.requestPermission(attachedDevice, pendingIntent);
                    }
                } else {
                    handleError(ErrorCode.NewDeviceNotSupported);
                }
            } else if (action.equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {
                if (usbStatus != UsbStatus.Connected)
                    return;

                UsbDevice detachedDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (detachedDevice == null)
                    return;
                if (detachedDevice.getDeviceId() == usbSerialDevice.getDeviceId()) {
                    usbStatus = UsbStatus.WaitingOnDevice;

                    if (dataHandler != null) {
                        dataHandler.pause();
                        dataHandlerPaused = true;
                    }

                    usbSerialDevice.close();
                    usbDeviceConnection.close();

                    handleStatusChange(StatusChange.UsbDisconnected);
                }
            }
        }
    };
}
