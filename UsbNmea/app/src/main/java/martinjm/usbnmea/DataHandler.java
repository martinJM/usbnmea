package martinjm.usbnmea;

import android.content.Context;
import android.content.Intent;

public abstract class DataHandler {
    protected DataHandlerCallback dataHandlerCallback;
    protected Context context;

    public interface ErrorCode{}

    public void create(Context contextIn, DataHandlerCallback dataHandlerCallbackIn) {
        if (dataHandlerCallback == null)
            dataHandlerCallback = dataHandlerCallbackIn;
        if (context == null)
            context = contextIn;
    }

    public void close() {}

    public void pause() {}
    public void resume() {}

    public abstract void handleIntent(Intent intent);
    public abstract void handleData(byte[] data);

    public void changeStatus(Object status) {}
    public Object getStatus(){
        return null;
    }
    public Object getData() { return null; }
}
