package martinjm.usbnmea;

import androidx.fragment.app.Fragment;

import martinjm.usbnmea.UsbConnection.*;

public abstract class DataHandlerSettings extends Fragment {

    // TODO: have this return if some buttons should be disabled
    public abstract void updateStatus(UsbStatus usbStatus, Object dataHandlerStatus);

    @Override
    public void onResume() {
        super.onResume();

        // This will make sure the buttons are set correctly.
        MainActivity main = (MainActivity) getActivity();
        if (main != null)
            main.updateStatus();
    }
}
