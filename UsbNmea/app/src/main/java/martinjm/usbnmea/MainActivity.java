package martinjm.usbnmea;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import martinjm.usbnmea.datahandlers.*;

public class MainActivity extends AppCompatActivity {

    private final SimpleDateFormat fileDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SS", Locale.getDefault());

    private SharedPreferences sharedPreferences;

    private static final String usbConnectionEnabledPreference = "usbConnectionEnabled";
    private static final String baudratePreference = "baudrate";
    private static final String dataHandlerPreference = "dataHandler";

    private static final int TESTBUF_LEN = 64;

    private UsbConnection.UsbConnectionBinder usbConnectionBinder = null;

    private static final int SAVE_ERRORS_REQUEST_CODE = 133;

    private static final HashMap<Class<? extends DataHandler>, Integer> availableDataHandlers = new HashMap<>();
    private static final HashMap<Class<? extends DataHandler>, Class<? extends DataHandlerSettings>> dataHandlerFragments = new HashMap<>();
    static {
        availableDataHandlers.put(EmptyHandler.class, R.string.DataHandlerEmpty);
        availableDataHandlers.put(Logger.class, R.string.DataHandlerFileLogger);
        availableDataHandlers.put(MockLocation.class, R.string.DataHandlerMockLocation);
        availableDataHandlers.put(Skyplot.class, R.string.DataHandlerSkyplot);

        dataHandlerFragments.put(EmptyHandler.class, EmptyHandlerFragment.class);
        dataHandlerFragments.put(Logger.class, LoggerFragment.class);
        dataHandlerFragments.put(MockLocation.class, EmptyHandlerFragment.class);
        dataHandlerFragments.put(Skyplot.class, SkyplotFragment.class);
    }
    private final HashMap<String, Class<? extends DataHandler>> revAvailableDataHandlers = new HashMap<>();
    private ArrayAdapter<String> arrayAdapterDataHandlers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // To prevent connecting a device from opening multiple instances, which is quite annoying
        if (!isTaskRoot() && getIntent() != null && getIntent().getAction() != null) {
            boolean c = getIntent().getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED);
            if (c) {
                finish();
                return;
            }
        }

        setContentView(R.layout.activity_main);

        // Disable the spinners changing by default to prevent mis-clicks from compromising data
        findViewById(R.id.spnBaudrate).setEnabled(false);
        findViewById(R.id.spnDataHandler).setEnabled(false);

        // Populate the data handler spinner
        arrayAdapterDataHandlers = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item);
        for (Class<? extends DataHandler> key : availableDataHandlers.keySet()) {
            @SuppressWarnings("ConstantConditions")
            String str = getString(availableDataHandlers.get(key));
            arrayAdapterDataHandlers.add(str);
            revAvailableDataHandlers.put(str, key);
        }
        ((Spinner) findViewById(R.id.spnDataHandler)).setAdapter(arrayAdapterDataHandlers);

        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbConnection.REPLIES.ERROR);
        filter.addAction(UsbConnection.REPLIES.STATUS_CHANGE);
        registerReceiver(usbConnectionReceiver, filter);

        // Set the preferences
        sharedPreferences = super.getPreferences(Context.MODE_PRIVATE);

        // Continue differently depending on if the usb connection should be on
        boolean usbConnectionEnabled = sharedPreferences.getBoolean(usbConnectionEnabledPreference, false);

        Log.d("usbNmeaDebugging", "usbConnectionEnabled onCreate: " + usbConnectionEnabled);

        if (usbConnectionEnabled) {
            bindService(
                    new Intent(this, UsbConnection.class),
                    serviceConnection,
                    Context.BIND_AUTO_CREATE
            );
        }

        // Load baudrate from shared preferences and set it in the spinner
        int baudrate = sharedPreferences.getInt(baudratePreference, 115200);
        String[] baudrates = getResources().getStringArray(R.array.baudrates);
        for (int i = 0; i < baudrates.length; i++) {
            if (Integer.parseInt(baudrates[i]) == baudrate) {
                ((Spinner) findViewById(R.id.spnBaudrate)).setSelection(i);
                break;
            }
        }

        // Load datahandler from shared preferences and set it in the spinner
        String dataHandler = sharedPreferences.getString(dataHandlerPreference, getString(R.string.DataHandlerEmpty));
        int pos = arrayAdapterDataHandlers.getPosition(dataHandler);
        ((Spinner) findViewById(R.id.spnDataHandler)).setSelection(pos);

        Log.d("usbNmeaDebugging", "Shared preference datahandler: " + dataHandler);

        ((Spinner) findViewById(R.id.spnBaudrate)).setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (usbConnectionBinder != null && usbConnectionBinder.pingBinder()) {
                            int baudrate = Integer.parseInt(String.valueOf(adapterView.getItemAtPosition(i)));

                            sharedPreferences.edit().putInt(baudratePreference, baudrate).apply();

                            if (baudrate != usbConnectionBinder.getService().getBaudrate()) {
                                usbConnectionBinder.getService().setBaudrate(baudrate);
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {}
                }
        );

        ((Spinner) findViewById(R.id.spnDataHandler)).setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        String newDataHandlerStr = String.valueOf(adapterView.getItemAtPosition(i));

                        sharedPreferences.edit().putString(dataHandlerPreference, newDataHandlerStr).apply();

                        if (revAvailableDataHandlers.containsKey(newDataHandlerStr)) {
                            Class<? extends DataHandler> newDataHandlerClass = revAvailableDataHandlers.get(newDataHandlerStr);
                            if (usbConnectionBinder != null && usbConnectionBinder.pingBinder()) {
                                if (usbConnectionBinder.getService().getDataHandler() != newDataHandlerClass)
                                    usbConnectionBinder.getService().setDataHandler(newDataHandlerClass);
                            }

                            Class<? extends DataHandlerSettings> newSettingsFragment = dataHandlerFragments.get(newDataHandlerClass);
                            if (newSettingsFragment == null)
                                newSettingsFragment = EmptyHandlerFragment.class;
                            Log.d("usbNmeaDebugging", "Setting new fragment" + newSettingsFragment);
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_datahandler_settings, newSettingsFragment, null)
                                    .commit();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {}
                }
        );
    }

    @Override
    protected void onDestroy() {
        if (usbConnectionBinder != null) {
            usbConnectionBinder = null;
            unbindService(serviceConnection);
        }

        if (isTaskRoot()) {
            unregisterReceiver(usbConnectionReceiver);
        }

        super.onDestroy();
    }

    public void updateStatus() {
        UsbConnection.UsbStatus usbStatus = UsbConnection.UsbStatus.Disconnected;
        Object dataHandlerStatus = null;

        if (usbConnectionBinder == null) {
            Log.d("usbNmeaDebugging", "updateStatus usbConnectionBinder null");

            findViewById(R.id.btnTestBaud).setEnabled(false);
            findViewById(R.id.btnStartUsb).setEnabled(true);
            findViewById(R.id.btnStopUsb).setEnabled(false);

            findViewById(R.id.spnBaudrate).setEnabled(false);
            findViewById(R.id.spnDataHandler).setEnabled(false);

            findViewById(R.id.chkBaudrateChangingEnabled).setEnabled(false);
            findViewById(R.id.chkBaudrateChangingEnabled).setSelected(false);
            findViewById(R.id.chkDataHandlerChangingEnabled).setEnabled(false);
            findViewById(R.id.chkDataHandlerChangingEnabled).setSelected(false);

            ((TextView) findViewById(R.id.tvUsbStatusDynamic)).setText(R.string.UsbStatusNotConnected);
        } else {
            if (!usbConnectionBinder.pingBinder()) {
                Log.d("usbNmeaDebugging", "updateStatus usbConnectionBinder noPing");

                usbConnectionBinder = null;
                unbindService(serviceConnection);

                findViewById(R.id.btnTestBaud).setEnabled(false);
                findViewById(R.id.btnStartUsb).setEnabled(true);
                findViewById(R.id.btnStopUsb).setEnabled(false);

                findViewById(R.id.spnBaudrate).setEnabled(false);
                findViewById(R.id.spnDataHandler).setEnabled(false);

                findViewById(R.id.chkBaudrateChangingEnabled).setEnabled(false);
                findViewById(R.id.chkBaudrateChangingEnabled).setSelected(false);
                findViewById(R.id.chkDataHandlerChangingEnabled).setEnabled(false);
                findViewById(R.id.chkDataHandlerChangingEnabled).setSelected(false);

                ((TextView) findViewById(R.id.tvUsbStatusDynamic)).setText(R.string.UsbStatusNotConnected);
            } else {
                Log.d("usbNmeaDebugging", "updateStatus usbConnectionBinder set");

                // Get statuses
                usbStatus = usbConnectionBinder.getService().getUsbStatus();
                Class<? extends DataHandler> dataHandler = usbConnectionBinder.getService().getDataHandler();
                int baudrate = usbConnectionBinder.getService().getBaudrate();
                ArrayList<UsbConnection.Message> messages = usbConnectionBinder.getService().getMessages();

                dataHandlerStatus = usbConnectionBinder.getService().getDataHandlerStatus();

                // Set button enabled statuses
                findViewById(R.id.btnTestBaud).setEnabled(usbStatus == UsbConnection.UsbStatus.Connected);
                findViewById(R.id.btnStartUsb).setEnabled(usbStatus == UsbConnection.UsbStatus.Disconnected);
                findViewById(R.id.btnStopUsb).setEnabled(usbStatus != UsbConnection.UsbStatus.Disconnected);

                findViewById(R.id.chkBaudrateChangingEnabled).setEnabled(true);
                findViewById(R.id.chkDataHandlerChangingEnabled).setEnabled(true);

                // Set usb status
                int usbStatusTextId;
                if (usbStatus == UsbConnection.UsbStatus.Connected) {
                    usbStatusTextId = R.string.UsbStatusConnected;
                } else if (usbStatus == UsbConnection.UsbStatus.WaitingOnDevice) {
                    usbStatusTextId = R.string.UsbStatusWaitingOnDevice;
                } else {
                    usbStatusTextId = R.string.UsbStatusDisconnected;
                }
                ((TextView) findViewById(R.id.tvUsbStatusDynamic)).setText(usbStatusTextId);

                // Set the baudrate
                String[] baudrates = getResources().getStringArray(R.array.baudrates);
                for (int i = 0; i < baudrates.length; i++) {
                    if (Integer.parseInt(baudrates[i]) == baudrate) {
                        ((Spinner) findViewById(R.id.spnBaudrate)).setSelection(i);
                        break;
                    }
                }

                // Set dataHandler spinner
                Log.d("DataHandler", "" + dataHandler);
                if (dataHandler != null) {
                    @SuppressWarnings("ConstantConditions")
                    String dataHandlerString = getString(availableDataHandlers.get(dataHandler));
                    int dataHandlerPosition = arrayAdapterDataHandlers.getPosition(dataHandlerString);
                    ((Spinner) findViewById(R.id.spnDataHandler)).setSelection(dataHandlerPosition);
                }

                // Set the messages
                StringBuilder newMessageContent = new StringBuilder();
                for (UsbConnection.Message m : messages) {
                    String code = "Unknown class error";
                    if (m.getType() == UsbConnection.ErrorMessage.class) {
                        code = ((UsbConnection.ErrorMessage) m).getErrorCode().toString();
                    } else if (m.getType() == UsbConnection.StatusChangeMessage.class) {
                        code = ((UsbConnection.StatusChangeMessage) m).getStatusChange().toString();
                    }

                    newMessageContent.append(
                            String.format(
                                    "(%s) %s\n",
                                    m.getTimestamp(),
                                    code
                            )
                    );
                }
                ((TextView) findViewById(R.id.tvMessagesDynamic)).setText(newMessageContent.toString());
            }
        }

        DataHandlerSettings dhs = (DataHandlerSettings) getSupportFragmentManager().findFragmentById(R.id.fragment_datahandler_settings);
        if (dhs != null)
            dhs.updateStatus(usbStatus, dataHandlerStatus);
        else
            Log.d("usbNmeaDebugging", "dhs null");
    }

    public void changeDataHandlerStatus(Object status) {
        if (usbConnectionBinder != null && usbConnectionBinder.pingBinder())
            usbConnectionBinder.getService().changeDataHandlerStatus(status);
    }

    public Object getDataHandlerData() {
        if (usbConnectionBinder != null && usbConnectionBinder.pingBinder())
            return usbConnectionBinder.getService().getDataHandlerData();
        return null;
    }

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            usbConnectionBinder = (UsbConnection.UsbConnectionBinder) iBinder;

            if (usbConnectionBinder.getService().getDataHandler() == null) {
                String newDataHandlerStr = String.valueOf(((Spinner) findViewById(R.id.spnDataHandler)).getSelectedItem());

                if (revAvailableDataHandlers.containsKey(newDataHandlerStr)) {
                    Class<? extends DataHandler> newDataHandlerClass = revAvailableDataHandlers.get(newDataHandlerStr);
                    usbConnectionBinder.getService().setDataHandler(newDataHandlerClass);
                }
            }

            updateStatus();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d("usbNmeaDebugging", "serviceConnection disconnected");

            usbConnectionBinder = null;

            updateStatus();
        }
    };

    private void serviceConnectionDisconnect() {
        usbConnectionBinder = null;
        unbindService(serviceConnection);
    }

    private final BroadcastReceiver usbConnectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == null)
                return;

            switch (intent.getAction()) {
                case UsbConnection.REPLIES.STATUS_CHANGE:
                    updateStatus();
                    break;
                case UsbConnection.REPLIES.ERROR:
                    UsbConnection.ErrorMessage errorMessage = (UsbConnection.ErrorMessage) intent.getSerializableExtra("error");
                    assert errorMessage != null;
                    ((TextView) findViewById(R.id.tvMessagesDynamic)).append(
                            String.format(
                                    "(%s) %s\n",
                                    errorMessage.getTimestamp(),
                                    errorMessage.getErrorCode()
                            )
                    );
                    break;
            }
        }
    };

    /*
     * GUI interaction handlers
     */

    public void onBaudrateChangeEnabledClick(View v) {
        findViewById(R.id.spnBaudrate).setEnabled(((CheckBox)v).isChecked());
    }

    public void onDataHandlerChangeEnabledClick(View v) {
        findViewById(R.id.spnDataHandler).setEnabled(((CheckBox)v).isChecked());
    }

    @SuppressWarnings("unused")
    public void onStartUsbClick(View v) {
        Log.d("usbNmeaDebugging", "onStartUsbClick");

        ((TextView) findViewById(R.id.tvUsbStatusDynamic)).setText(getString(R.string.Starting));
        findViewById(R.id.btnStartUsb).setEnabled(false);

        int baudrate = Integer.parseInt(
                String.valueOf(
                        ((Spinner) findViewById(R.id.spnBaudrate)).getSelectedItem()
                )
        );

        Log.d("usbNmeaDebugging", "Starting baudrate: " + baudrate);

        startService(
                new Intent(this, UsbConnection.class)
                        .setAction(UsbConnection.REQUESTS.START_USB)
                        .putExtra("baudrate", baudrate)
        );

        bindService(
                new Intent(this, UsbConnection.class),
                serviceConnection,
                Context.BIND_AUTO_CREATE
        );

        sharedPreferences.edit().putBoolean(usbConnectionEnabledPreference, true).apply();
    }

    @SuppressWarnings("unused")
    public void onStopUsbClick(@SuppressWarnings("unused") View v) {
        ((TextView) findViewById(R.id.tvUsbStatusDynamic)).setText(getString(R.string.Stopping));
        findViewById(R.id.btnStopUsb).setEnabled(false);

        startService(
                new Intent(this, UsbConnection.class)
                        .setAction(UsbConnection.REQUESTS.STOP_USB)
        );

        serviceConnectionDisconnect();

        sharedPreferences.edit().putBoolean(usbConnectionEnabledPreference, false).apply();
    }

    public void onTestBaudClick(@SuppressWarnings("unused") View v) {
        if (usbConnectionBinder != null && usbConnectionBinder.pingBinder()) {
            // TODO: change from busy wait to callback method system

            byte[] testbuf = new byte[TESTBUF_LEN];
            byte[] previous_message = null;
            int testbuf_len = 0;

            while (testbuf_len < TESTBUF_LEN) {
                byte[] last_message = usbConnectionBinder.getService().getLastMessage();
                if (Arrays.equals(last_message, previous_message)) {
                    // This is a busy wait, which doesn't seem to impact the application
                    // too much, but it is still a waste.
                    continue;
                }

                int copylen = last_message.length;
                if (testbuf_len + copylen > TESTBUF_LEN)
                    copylen = TESTBUF_LEN - testbuf_len;

                System.arraycopy(last_message, 0, testbuf, testbuf_len, copylen);

                testbuf_len += copylen;
                previous_message = last_message;
            }

            String message = new String(testbuf);
            Toast.makeText(this, "Test data: " + message, Toast.LENGTH_SHORT).show();
        } else {
            // TODO: show message?
            Log.d("usbNmeaDebugging", "usbConnectionBinder not connected");
            updateStatus();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SAVE_ERRORS_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            Uri errorUri = data.getData();

            if (errorUri == null) {
                Toast.makeText(getApplicationContext(), R.string.MessageSavingFailed, Toast.LENGTH_SHORT).show();
                return;
            }

            try{
                OutputStream outputStream = getContentResolver().openOutputStream(errorUri);

                if (outputStream == null) {
                    Toast.makeText(getApplicationContext(), R.string.MessageSavingFailed, Toast.LENGTH_SHORT).show();
                    return;
                }

                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);

                outputStreamWriter.write(((TextView) findViewById(R.id.tvMessagesDynamic)).getText().toString());

                outputStreamWriter.close();
                outputStream.close();

                Toast.makeText(getApplicationContext(), R.string.MessageSavingSuccess, Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), R.string.MessageSavingFailed, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressWarnings("unused")
    public void onSaveErrorClick(View v) {
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/*");
        intent.putExtra(Intent.EXTRA_TITLE, "errors_" + fileDateFormat.format(Calendar.getInstance().getTime()) + ".txt");

        // TODO: change when android docs are updated:
        //       https://developer.android.com/training/data-storage/shared/documents-files#create-file
        startActivityForResult(intent, SAVE_ERRORS_REQUEST_CODE);
    }

    public void onClearErrorClick(@SuppressWarnings("unused") View v) {
        if (usbConnectionBinder != null && usbConnectionBinder.pingBinder()) {
            usbConnectionBinder.getService().clearMessages();
            updateStatus();
        } else {
            ((TextView) findViewById(R.id.tvMessagesDynamic)).setText("");
        }
    }
}
