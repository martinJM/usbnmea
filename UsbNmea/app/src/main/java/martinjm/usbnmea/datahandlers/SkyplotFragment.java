package martinjm.usbnmea.datahandlers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Calendar;
import java.util.Date;

import martinjm.usbnmea.DataHandlerSettings;
import martinjm.usbnmea.MainActivity;
import martinjm.usbnmea.R;
import martinjm.usbnmea.UsbConnection;

public class SkyplotFragment extends DataHandlerSettings {

    private MainActivity main;

    // TODO: make this user settable?
    private final int labelUpdateIntervalMillisec = 50;
    private Date lastUpdate;

    private final Handler handler = new Handler(Looper.getMainLooper());
    private Runnable runnable;

    private final BroadcastReceiver skyplotIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == null)
                return;

            if (intent.getAction().equals(Skyplot.REPLIES.RECEIVED_LOCATION)) {
                if (main == null)
                    return;

                Object dataHandlerData = main.getDataHandlerData();

                if (dataHandlerData == null) {
                    // TODO: handle properly?
                    Log.w("UsbNmea", "Data handler status is null");
                    return;
                }

                if (!Skyplot.SatelliteData.class.isAssignableFrom(dataHandlerData.getClass())) {
                    // TODO: handle properly?
                    Log.w("UsbNmea", "Data handler status is not of type satellite data");
                    return;
                }

                Skyplot.SatelliteData satelliteData = (Skyplot.SatelliteData) dataHandlerData;
                SkyplotView skyplotView = main.findViewById(R.id.skyplotView);
                skyplotView.setSatellites(satelliteData.viewedSatellites, satelliteData.activeSatellites);
                skyplotView.postInvalidate();

                lastUpdate = Calendar.getInstance().getTime();
                ((TextView) main.findViewById(R.id.tvSkyplotAgeDynamic)).setText(String.valueOf(0));
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_skyplot, container, false);

        IntentFilter filter = new IntentFilter();
        filter.addAction(Skyplot.REPLIES.RECEIVED_LOCATION);

        main = (MainActivity) getActivity();
        if (main != null)
            main.registerReceiver(skyplotIntentReceiver, filter);

        if (runnable != null)
            handler.removeCallbacks(runnable);
        runnable = this::updateAgeLabel;
        handler.postDelayed(runnable, labelUpdateIntervalMillisec);

        ((TextView) v.findViewById(R.id.tvSkyplotAgeDynamic)).setText("");

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (runnable != null)
            handler.removeCallbacks(runnable);
    }

    @Override
    public void updateStatus(UsbConnection.UsbStatus usbStatus, Object dataHandlerStatus) {
        // Nothing to do here
    }

    private void updateAgeLabel() {
        if (lastUpdate != null) {
            Date now = Calendar.getInstance().getTime();
            long diff = (now.getTime() - lastUpdate.getTime());
            ((TextView) main.findViewById(R.id.tvSkyplotAgeDynamic)).setText(String.valueOf(diff));
        }

        handler.postDelayed(runnable, labelUpdateIntervalMillisec);
    }
}
