package martinjm.usbnmea.datahandlers;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import martinjm.usbnmea.DataHandler;

public class Logger extends DataHandler {

    private boolean logging = false;

    private Uri uri = null;

    private OutputStream outputStream;

    public enum LogStatus {
        Logging,
        NotLogging
    }

    enum ErrorCode implements DataHandler.ErrorCode {
        CloseStreamError,
        NullStream,
        OpenStreamError,
        ReopenStreamError,
        WriteError
    }

    static final class REQUESTS {
        /*
         * Expects no extras
         */
        static final String START_LOGGING = "martinjm.usbnmea.datahandlers.logger.start_logging";

        /*
         * Expects no extras
         */
        static final String STOP_LOGGING = "martinjm.usbnmea.datahandlers.logger.stop_logging";
    }

    static final class REPLIES {
        /*
         * Contains no extras
         */
        static final String STATUS_CHANGE = "martinjm.usbnmea.datahandlers.logger.status_change";

        /*
         * Contains no extras
         */
        static final String ERROR = "martinjm.usbnmea.datahandlers.logger.error";
    }

    static final class Status {
        LogStatus logStatus;
        Uri fileUri;
    }

    private void startLogging() {
        try{
            outputStream = context.getContentResolver().openOutputStream(uri, "wa");
        } catch(FileNotFoundException e) {
            dataHandlerCallback.handleError(
                    ErrorCode.OpenStreamError,
                    new Intent(REPLIES.ERROR)
            );
            return;
        }

        logging = true;

        dataHandlerCallback.sendBroadcast(new Intent(REPLIES.STATUS_CHANGE));
    }

    private void stopLogging() {
        logging = false;

        try{
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            dataHandlerCallback.handleError(
                    ErrorCode.CloseStreamError,
                    new Intent(REPLIES.ERROR)
            );
        }

        dataHandlerCallback.sendBroadcast(new Intent(REPLIES.STATUS_CHANGE));
    }

    @Override
    public void pause() {
        if (logging) {
            try {
                outputStream.flush();
                outputStream.close();
            } catch (IOException e) {
                dataHandlerCallback.handleError(
                        ErrorCode.CloseStreamError,
                        new Intent(REPLIES.ERROR)
                );
            }
        }
    }

    @Override
    public void resume() {
        if (logging) {
            try {
                outputStream = context.getContentResolver().openOutputStream(uri, "wa");
            } catch (IOException e) {
                dataHandlerCallback.handleError(
                        ErrorCode.ReopenStreamError,
                        new Intent(REPLIES.ERROR)
                );
            }
        }
    }

    @Override
    public void close() {
        if (logging) {
            try {
                outputStream.flush();
                outputStream.close();
            } catch (IOException e) {
                // Just an attempt to close the stream properly
            }
        }
    }

    @Override
    public void handleIntent(Intent intent) {
        String action = intent.getAction();
        if (action == null)
            return;
        switch (action) {
            case REQUESTS.START_LOGGING:
                Uri intentUri = intent.getParcelableExtra("outputFileUri");
                if (intentUri != null)
                    uri = intentUri;
                startLogging();
                break;
            case REQUESTS.STOP_LOGGING:
                stopLogging();
                break;
        }
    }

    @Override
    public void handleData(byte[] data) {
        if (logging) {
            if (outputStream == null) {
                dataHandlerCallback.handleError(
                        ErrorCode.NullStream,
                        new Intent(REPLIES.ERROR)
                );
                return;
            }

            try {
                outputStream.write(data);
            } catch (IOException e) {
                dataHandlerCallback.handleError(
                        ErrorCode.WriteError,
                        new Intent(REPLIES.ERROR)
                );
            }
        }
    }

    @Override
    public void changeStatus(Object status) {
        if (status != null && Status.class.isAssignableFrom(status.getClass())) {
            Status real_status = (Status) status;

            if (real_status.fileUri != null)
                uri = real_status.fileUri;

            if (real_status.logStatus != null) {
                if (real_status.logStatus == LogStatus.Logging) {
                    startLogging();
                } else {
                    stopLogging();
                }
            }
        }
    }

    @Override
    public Object getStatus() {
        Status status = new Status();

        if (logging)
            status.logStatus = LogStatus.Logging;
        else
            status.logStatus = LogStatus.NotLogging;

        Log.d("usbNmeaDebugging", "Get status uri: " + uri);

        status.fileUri = uri;

        return status;
    }
}
