package martinjm.usbnmea.datahandlers;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import martinjm.usbnmea.DataHandler;
import martinjm.usbnmea.DataHandlerCallback;

public class Skyplot extends DataHandler {

    static final class REPLIES {
        /*
         * Contains no extras
         */
        static final String RECEIVED_LOCATION = "martinjm.usbnmea.datahandlers.skyplot.received_location";
    }

    private final ArrayList<Byte> buffer = new ArrayList<>(256);  // Capacity should suffice

    private SatelliteData satelliteData;

    private NmeaParser nmeaParser;
    private final NmeaParser.NmeaParserCallback nmeaParserCallback = new NmeaParser.NmeaParserCallback() {
        @Override
        void onLocationSatelliteData(Integer[] activeSatellites, NmeaParser.Satellite[] viewedSatellites) {
            satelliteData = new SatelliteData();
            satelliteData.activeSatellites = activeSatellites;
            satelliteData.viewedSatellites = viewedSatellites;

            dataHandlerCallback.sendBroadcast(new Intent(REPLIES.RECEIVED_LOCATION));
        }
    };

    static final class SatelliteData {
        NmeaParser.Satellite[] viewedSatellites;
        Integer[] activeSatellites;
    }

    @Override
    public void create(Context contextIn, DataHandlerCallback dataHandlerCallbackIn) {
        super.create(contextIn, dataHandlerCallbackIn);

        nmeaParser = new NmeaParser(nmeaParserCallback, "none");
    }

    @Override
    public void handleIntent(Intent intent) {
        // This data handler doesn't use intents
    }

    @Override
    public void handleData(byte[] data) {
        // Add data to buffer
        for (byte b : data) {
            buffer.add(b);
        }

        // While buffer contains newline, handle message
        while (buffer.indexOf((byte) '\n') != -1) {
            int newlinePos = buffer.indexOf((byte) '\n');
            List<Byte> messageList = buffer.subList(0, newlinePos);

            handleMessage(messageList);

            messageList.clear();
            // Also remove newline
            buffer.remove(0);
        }
    }

    private void handleMessage(List<Byte> messageList) {
        if (messageList.size() == 0)
            return;

        if (messageList.get(0) != '$')
            return;

        byte[] messageArray = new byte[messageList.size()];
        int i = 0;
        for (Byte b : messageList)
            messageArray[i++] = b;
        String message = new String(messageArray);

        nmeaParser.parseMessage(message);
    }

    @Override
    public Object getData() {
        return satelliteData;
    }
}
