package martinjm.usbnmea.datahandlers;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import martinjm.usbnmea.DataHandler;
import martinjm.usbnmea.DataHandlerCallback;

public class MockLocation extends DataHandler {

    private static final String MockLocationName = LocationManager.GPS_PROVIDER;

    private final ArrayList<Byte> buffer = new ArrayList<>(256);  // Capacity should suffice

    private NmeaParser nmeaParser;
    private final NmeaParser.NmeaParserCallback nmeaCallback = new NmeaParser.NmeaParserCallback() {
        @Override
        void onLocation(NmeaParser.ParsedMessages parsedMessages, Location location) {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            assert locationManager != null;
            Log.d("UsbNmeaDebugging", "ParsedMessages: " + parsedMessages.toString());
            if (parsedMessages.gga && parsedMessages.rmc)
                locationManager.setTestProviderLocation(MockLocationName, location);
        }
    };

    @Override
    public void create(Context contextIn, DataHandlerCallback dataHandlerCallbackIn) {
        super.create(contextIn, dataHandlerCallbackIn);

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        try {
            locationManager.addTestProvider(
                    MockLocationName,
                    false,
                    false,
                    false,
                    false,
                    true,
                    true,
                    true,
                    3,  // 1 = low, 2 = medium, 3 = high
                    1           // 1 = fine, 2 = coarse
            );
        } catch (IllegalArgumentException e) {
            // Provider already exists
        }
        locationManager.setTestProviderEnabled(MockLocationName, true);

        nmeaParser = new NmeaParser(nmeaCallback, MockLocationName);
    }

    @Override
    public void close() {
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            assert locationManager != null;

            locationManager.setTestProviderEnabled(MockLocationName, false);
            locationManager.removeTestProvider(MockLocationName);
        } catch (IllegalArgumentException e) {
            // Provider is not known
        }
    }

    @Override
    public void handleIntent(Intent intent) {
        // This data handler doesn't use intents
    }

    @Override
    public void handleData(byte[] data) {
        // Add data to buffer
        for (byte b : data) {
            buffer.add(b);
        }

        // While buffer contains newline, handle message
        while (buffer.indexOf((byte) '\n') != -1) {
            int newlinePos = buffer.indexOf((byte) '\n');
            List<Byte> messageList = buffer.subList(0, newlinePos);

            handleMessage(messageList);

            messageList.clear();
            // Also remove newline
            buffer.remove(0);
        }
    }

    private void handleMessage(List<Byte> messageList) {
        if (messageList.size() == 0)
            return;

        if (messageList.get(0) != '$')
            return;

        byte[] messageArray = new byte[messageList.size()];
        int i = 0;
        for (Byte b : messageList)
            messageArray[i++] = b;
        String message = new String(messageArray);

        nmeaParser.parseMessage(message);
    }
}
