package martinjm.usbnmea.datahandlers;

import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.Nullable;

import com.felhr.utils.HexData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Pattern;

class NmeaParser {

    // This class expects the data in chronological order
    // That is, it may not give the desired result if the timestamps jump around

    private static final class Patterns {
        static final Pattern GGA_COMMAND = Pattern.compile("[$]G[NP]GGA");
        static final Pattern GSA_COMMAND = Pattern.compile("[$]G[PLAB]GSA");
        static final Pattern GST_COMMAND = Pattern.compile("[$]G[NPLAB]GST");
        static final Pattern GSV_COMMAND = Pattern.compile("[$]G[PLAB]GSV");
        static final Pattern RMC_COMMAND = Pattern.compile("[$]G[NP]RMC");
        static final Pattern ZDA_COMMAND = Pattern.compile("[$]GNZDA");

        static final Pattern MESSAGE_PATTERN = Pattern.compile("^[$].*,.*[*][0-9a-fA-F]{2}$");
    }

    public static class ParsedMessages {
        boolean gga;
        boolean gsa;
        boolean gst;
        boolean gsv;
        boolean rmc;
        boolean zda;

        ParsedMessages(boolean zda) {
            this.gga = false;
            this.gsa = false;
            this.gst = false;
            this.gsv = false;
            this.rmc = false;
            this.zda = zda;
        }

        ParsedMessages(boolean gga, boolean rmc, boolean zda) {
            this.gga = gga;
            this.gsa = false;
            this.gst = false;
            this.gsv = false;
            this.rmc = rmc;
            this.zda = zda;
        }

        ParsedMessages(boolean gga, boolean gsa, boolean gst, boolean gsv, boolean rmc, boolean zda) {
            this.gga = gga;
            this.gsa = gsa;
            this.gst = gst;
            this.gsv = gsv;
            this.rmc = rmc;
            this.zda = zda;
        }

        boolean any() {
            return gga | gsa | gst | gsv | rmc | zda;
        }

        @Override
        public boolean equals(@Nullable Object obj) {
            if (obj == null) {
                return false;
            } else if (obj.getClass() != this.getClass()) {
                return false;
            } else {
                ParsedMessages other = (ParsedMessages) obj;
                return
                        other.gga == this.gga &&
                        other.gsa == this.gsa &&
                        other.gst == this.gst &&
                        other.gsv == this.gsv &&
                        other.rmc == this.rmc &&
                        other.zda == this.zda;
            }
        }

        @Override
        public String toString() {
            return "ParsedMessages{" +
                    "gga=" + gga +
                    ", gsa=" + gsa +
                    ", gst=" + gst +
                    ", gsv=" + gsv +
                    ", rmc=" + rmc +
                    ", zda=" + zda +
                    '}';
        }
    }

    static final class Constellation {
        static final int ANY = 0;
        static final int GPS = 1;
        static final int GLONASS = 2;
        static final int GALILEO = 3;
        static final int BEIDOU = 4;

        static final int count = 5;
    }

    public static class Satellite {
        int constellation;
        int idNum;
        int elevationDeg;
        int azimuthDeg;
        int snrDb;

        Satellite() {}

        Satellite(int constellation, int idNum, int elevationDeg, int azimuthDeg, int snrDb) {
            this.constellation = constellation;
            this.idNum = idNum;
            this.elevationDeg = elevationDeg;
            this.azimuthDeg = azimuthDeg;
            this.snrDb = snrDb;
        }

        @Override
        public boolean equals(@Nullable Object obj) {
            if (obj == null) {
                return false;
            } else if (obj.getClass() != this.getClass()) {
                return false;
            } else {
                Satellite other = (Satellite) obj;
                return
                        other.idNum == this.idNum &&
                        other.constellation == this.constellation &&
                        other.elevationDeg == this.elevationDeg &&
                        other.azimuthDeg == this.azimuthDeg &&
                        other.snrDb == this.snrDb;
            }
        }
    }

    @SuppressWarnings({"unused", "EmptyMethod"})
    static abstract class NmeaParserCallback {

        void onLocation(ParsedMessages parsedMessages, Location location) {}
        void onLocationSatelliteData(Integer[] activeSatellites, Satellite[] viewedSatellites) {}

        void onNonImplementedMessage(String message) {}

        void onIncorrectChecksum(String message) {}
        void onIncorrectMessage(String message) {}
        void onIgnoredMessage(String message) {}

        void onMissingSatellite() {}
    }

    private final String mockLocationName;
    private NmeaParserCallback callback = new NmeaParserCallback() {};

    private long currentDate = -1;
    private long currentTimestamp = -1;

    private Location location = null;
    private ParsedMessages parsedMessages = new ParsedMessages(false);

    private static final float KNOTS_TO_MS = 0.514444f; // MS is Meters per Second

    // TODO: improve accuracy measure
    // GST messages will likely be better anyway, so I may not implement something better
    // The value 4.7 is taken from here: https://doc.arcgis.com/en/survey123/desktop/get-answers/high-accuracy-use.htm#ESRI_SECTION3_ED182C2B3F9B4BE5BC3011FFE045B5E6
    // This estimate seems to be too low. I'm often outside the region, albeit indoors.
    private static final float HORIZONTAL_ACCURACY_MULTIPLIER = 4.7f;
    private static final float VERTICAL_ACCURACY_MULTIPLIER = 4.7f;

    private final Handler handler = new Handler(Looper.getMainLooper());
    private Runnable runnable;
    // TODO: make this user-settable
    //       This will also need a manner in which to see that a message was prematurely sent
    private static final int CompleteDelayMillisec = 100;

    private final List<Integer> activeSatellites = new ArrayList<>();
    private final List<Satellite> viewedSatellites = new ArrayList<>();
    private int[] viewedSatellitesCounter = new int[Constellation.count];

    private String lastCommandType = "";

    NmeaParser(NmeaParserCallback callback, String mockLocationProviderName) {
        if (callback != null)
            this.callback = callback;
        this.mockLocationName = mockLocationProviderName;
    }

    private byte calculateChecksum(String message) {
        byte checksum = 0;
        for (int i = 1; i < message.indexOf("*"); i++)
            checksum ^= message.charAt(i);
        return checksum;
    }

    void parseMessage(String message) {
        if (!Patterns.MESSAGE_PATTERN.matcher(message).find()) {
            callback.onIncorrectMessage(message);
            return;
        }

        String command = message.substring(0, message.indexOf(","));

        try {
            int checksumStart = message.indexOf("*") + 1;
            String checksumHex = message.substring(checksumStart, checksumStart + 2);
            byte checksum = HexData.stringTobytes(checksumHex)[0];
            if (calculateChecksum(message) != checksum) {
                callback.onIncorrectChecksum(message);
                return;
            }
        } catch (NumberFormatException e) {
            callback.onIncorrectChecksum(message);
            return;
        }

        if (Patterns.GGA_COMMAND.matcher(command).find()) {
            parseGGA(message);
        } else if (Patterns.GSA_COMMAND.matcher(command).find()) {
            parseGSA(message);
        } else if (Patterns.GST_COMMAND.matcher(command).find()) {
            parseGST(message);
        } else if (Patterns.GSV_COMMAND.matcher(command).find()) {
            parseGSV(message);
        } else if (Patterns.RMC_COMMAND.matcher(command).find()) {
            parseRMC(message);
        } else if (Patterns.ZDA_COMMAND.matcher(command).find()) {
            parseZDA(message);
        } else {
            callback.onNonImplementedMessage(message);
        }

        lastCommandType = command.substring(3, 6);

        checkComplete();
    }

    private double toDegrees(double input, boolean nonNegate) {
        double degrees = Math.floor(input / 100);
        double minutes = input - (degrees * 100);

        double scalar = 1;
        if (!nonNegate)
            scalar = -1;

        return scalar * (degrees + (minutes / 60));
    }

    private void checkTime(long newTime) {
        if (newTime != currentTimestamp) {
            if (parsedMessages.any())
                sendLocation();

            currentTimestamp = newTime;
        }

        if (location == null)
            location = new Location(mockLocationName);
    }

    private void checkComplete() {
        if (runnable != null)
            handler.removeCallbacks(runnable);
        runnable = this::lastMessageTimeout;
        handler.postDelayed(runnable, CompleteDelayMillisec);
    }

    private void lastMessageTimeout() {
        if (parsedMessages.any()) {
            sendLocation();
        }
    }

    private void sendLocation() {
        if (location == null)
            return;

        if (runnable != null)
            handler.removeCallbacks(runnable);

        if (parsedMessages.zda)
            location.setTime(currentDate + currentTimestamp);
        else
            location.setTime(System.currentTimeMillis());
        location.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());

        callback.onLocation(parsedMessages, location);

        if (parsedMessages.gsa || parsedMessages.gsv)
            callback.onLocationSatelliteData(
                activeSatellites.toArray(new Integer[0]),
                viewedSatellites.toArray(new Satellite[0])
            );

        location = new Location(mockLocationName);
        parsedMessages = new ParsedMessages(parsedMessages.zda);
    }

    private void parseGGA(String message) {
        /*
            Message format:
             1) Time (UTC)
             2) Latitude
             3) N or S (North or South)
             4) Longitude
             5) E or W (East or West)
             6) GPS Quality Indicator,
                0 - fix not available,
                1 - GPS fix,
                2 - Differential GPS fix
             7) Number of satellites in view, 00 - 12
             8) Horizontal Dilution of precision
             9) Antenna Altitude above/below mean-sea-level (geoid)
            10) Units of antenna altitude, meters
            11) Geoidal separation, the difference between the WGS-84 earth ellipsoid and mean-sea-level (geoid), "-" means mean-sea-level below ellipsoid
            12) Units of geoidal separation, meters
            13) Age of differential GPS data, time in seconds since last SC104 type 1 or 9 update, null field when DGPS is not used
            14) Differential reference station ID, 0000-1023
            15) Checksum
         */

        String[] splitMessage = message.split(",");
        splitMessage[splitMessage.length-1] = splitMessage[splitMessage.length-1].split("[*]")[0];

        if (splitMessage.length != 15) {
            callback.onIncorrectMessage(message);
            return;
        }

        // Ignore if there is no fix
        if (splitMessage[6].equals("0")) {
            callback.onIgnoredMessage(message);
            return;
        }

        // Check if the units are right
        if (!splitMessage[10].equals("M") || !splitMessage[12].equals("M")) {
            callback.onIncorrectMessage(message);
            return;
        }

        long time;
        double lat;
        double lon;
        int satCount;
        float hdop;
        double alt;
        double altSep;

        try {
            time = Long.parseLong(splitMessage[1].substring(0, splitMessage[1].indexOf('.')));
            lat = Double.parseDouble(splitMessage[2]);
            lon = Double.parseDouble(splitMessage[4]);
            satCount = Integer.parseInt(splitMessage[7]);
            hdop = Float.parseFloat(splitMessage[8]);
            alt = Double.parseDouble(splitMessage[9]);
            altSep = Double.parseDouble(splitMessage[11]);
        } catch (NumberFormatException e) {
            callback.onIncorrectMessage(message);
            return;
        }

        checkTime(time);

        lat = toDegrees(lat, splitMessage[3].equals("N"));
        lon = toDegrees(lon, splitMessage[5].equals("E"));

        location.setTime(time);
        location.setLatitude(lat);
        location.setLongitude(lon);

        // Not all apps use this, but it cannot hurt to set it
        Bundle extras = new Bundle();
        extras.putInt("satellites", satCount);
        location.setExtras(extras);

        if (!parsedMessages.gst)
            location.setAccuracy(hdop * HORIZONTAL_ACCURACY_MULTIPLIER);

        location.setAltitude(alt + altSep);

        parsedMessages.gga = true;
    }

    private void parseGSA(String message) {
        /*
            Message format:
             1) Selection mode (M=Manual, forced to operate in 2D or 3D, A=Automatic, 2D/3D)
             2) Mode (1 = no fix, 2 = 2D fix, 3 = 3D fix)
             3) ID of 1st satellite used for fix
             4) ID of 2nd satellite used for fix
             ...
            14) ID of 12th satellite used for fix
            15) PDOP in meters
            16) HDOP in meters
            17) VDOP in meters
            18) Checksum
         */

        if (!lastCommandType.equals("GSA")) {
            activeSatellites.clear();
        }

        String[] splitMessage = message.split(",");
        splitMessage[splitMessage.length-1] = splitMessage[splitMessage.length-1].split("[*]")[0];

        if (splitMessage.length != 18) {
            callback.onIncorrectMessage(message);
            return;
        }

        int mode;
        float hdop;
        float vdop;

        try {
            mode = Integer.parseInt(splitMessage[2]);
            // Ignore no fix for now
            if (mode == 1)
                return;

            hdop = Float.parseFloat(splitMessage[16]);
            vdop = Float.parseFloat(splitMessage[17]);

            for (int i = 3; i < 15; i++) {
                if (splitMessage[i].equals(""))
                    break;

                int satelliteId = Integer.parseInt(splitMessage[i]);
                activeSatellites.add(satelliteId);
            }
        } catch (NumberFormatException e) {
            callback.onIncorrectMessage(message);
            return;
        }

        Log.d("UsbNmeaDebugging", "Active satellites size: " + activeSatellites.size());

        if (location == null)
            location = new Location(mockLocationName);

        if (!parsedMessages.gst) {
            location.setAccuracy(hdop * HORIZONTAL_ACCURACY_MULTIPLIER);
            if (mode == 3 && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                location.setVerticalAccuracyMeters(vdop * VERTICAL_ACCURACY_MULTIPLIER);
        }

        parsedMessages.gsa = true;
    }

    private void parseGST(String message) {
        /*
            Message format:
             1) TC time of associated GGA fix
             2) Total RMS standard deviation of ranges inputs to the navigation solution
             3) Standard deviation (meters) of semi-major axis of error ellipse
             4) Standard deviation (meters) of semi-minor axis of error ellipse
             5) Orientation of semi-major axis of error ellipse (true north degrees)
             6) Standard deviation (meters) of latitude error
             7) Standard deviation (meters) of longitude error
             8) Standard deviation (meters) of altitude error
             9) Checksum
         */

        String[] splitMessage = message.split(",");
        splitMessage[splitMessage.length-1] = splitMessage[splitMessage.length-1].split("[*]")[0];

        if (splitMessage.length != 9) {
            callback.onIncorrectMessage(message);
            return;
        }

        long time;
        float latError;
        float lonError;
        float altError;

        try {
            time = Long.parseLong(splitMessage[1].substring(0, splitMessage[1].indexOf('.')));
            latError = Float.parseFloat(splitMessage[6]);
            lonError = Float.parseFloat(splitMessage[7]);
            altError = Float.parseFloat(splitMessage[8]);
        } catch (NumberFormatException e) {
            callback.onIncorrectMessage(message);
            return;
        }

        checkTime(time);

        location.setAccuracy(Math.max(latError, lonError));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            location.setVerticalAccuracyMeters(altError);

        parsedMessages.gst = true;
    }

    private void parseGSV(String message) {
        /*
            Message format:
             1) total number of messages
             2) message number
             3) satellites in view
             4) satellite number
             5) elevation in degrees
             6) azimuth in degrees to true
             7) SNR in dB
             more satellite infos like 4)-7)
             n) Checksum
         */

        if (!lastCommandType.equals("GSV")) {
            viewedSatellites.clear();
            viewedSatellitesCounter = new int[Constellation.count];
        }

        String[] splitMessage = message.split(",");
        splitMessage[splitMessage.length-1] = splitMessage[splitMessage.length-1].split("[*]")[0];

        if (splitMessage.length < 4) {
            callback.onIncorrectMessage(message);
            return;
        }

        int constellation = Constellation.ANY;

        if (splitMessage[0].contains("GP")) {
            constellation = Constellation.GPS;
        } else if (splitMessage[0].contains("GL")) {
            constellation = Constellation.GLONASS;
        } else if (splitMessage[0].contains("GA")) {
            constellation = Constellation.GALILEO;
        } else if (splitMessage[0].contains("GB")) {
            constellation = Constellation.BEIDOU;
        }

        int messageCount;
        int messageNum;
        int satCount;

        try {
            messageCount = Integer.parseInt(splitMessage[1]);
            messageNum = Integer.parseInt(splitMessage[2]);
            satCount = Integer.parseInt(splitMessage[3]);
        } catch (NumberFormatException e) {
            callback.onIncorrectMessage(message);
            return;
        }

        int satsToParse = satCount - viewedSatellitesCounter[constellation];
        if (satsToParse > 4)
            satsToParse = 4;

        for (int i = 0; i < satsToParse; i++) {
            int startIndex = 4 + i * 4;

            Satellite satellite = new Satellite();
            satellite.constellation = constellation;
            try {
                satellite.idNum = Integer.parseInt(splitMessage[startIndex]);

                if (splitMessage[startIndex + 1].equals(""))
                    continue;
                else
                    satellite.elevationDeg = Integer.parseInt(splitMessage[startIndex + 1]);

                if (splitMessage[startIndex + 2].equals(""))
                    continue;
                else
                    satellite.azimuthDeg = Integer.parseInt(splitMessage[startIndex + 2]);

                if (splitMessage[startIndex + 3].equals(""))
                    satellite.snrDb = -1;
                else
                    satellite.snrDb = Integer.parseInt(splitMessage[startIndex + 3]);
            } catch (NumberFormatException e) {
                callback.onIncorrectMessage(message);
                return;
            } catch (ArrayIndexOutOfBoundsException e) {
                callback.onIncorrectMessage(message);
                Log.w("UsbNmea", "GSV Array out of bounds exception: " + message);
                return;
            }
            viewedSatellites.add(satellite);
            viewedSatellitesCounter[constellation] += 1;
        }

        if (messageCount == messageNum && viewedSatellitesCounter[constellation] != satCount) {
            callback.onMissingSatellite();
        }

        parsedMessages.gsv = true;
    }

    private void parseRMC(String message) {
        /*
            Message format:
             1) Time (UTC)
             2) Status, V = Navigation receiver warning
             3) Latitude
             4) N or S
             5) Longitude
             6) E or W
             7) Speed over ground, knots
             8) Track made good, degrees true
             9) Date, ddmmyy
            10) Magnetic Variation, degrees
            11) E or W
            12) Checksum
        */

        String[] splitMessage = message.split(",");
        splitMessage[splitMessage.length-1] = splitMessage[splitMessage.length-1].split("[*]")[0];

        // My antenna adds an additional field right before the checksum.
        // The value seems to be 'A' all the time, but the usage is unknown at this point.
        if (splitMessage.length != 12 && splitMessage.length != 13) {
            callback.onIncorrectMessage(message);
            return;
        }

        // Ignore invalid data
        if (splitMessage[2].equals("V")) {
            callback.onIgnoredMessage(message);
            return;
        }

        boolean hasSpeed = !splitMessage[7].equals("");
        boolean hasBearing = !splitMessage[8].equals("");

        long time;
        double lat;
        double lon;
        float speed = 0.0f;
        float bearing = 0.0f;

        try {
            time = Long.parseLong(splitMessage[1].substring(0, splitMessage[1].indexOf('.')));
            lat = Double.parseDouble(splitMessage[3]);
            lon = Double.parseDouble(splitMessage[5]);
            if (hasSpeed)
                speed = Float.parseFloat(splitMessage[7]);
            if (hasBearing)
                bearing = Float.parseFloat(splitMessage[8]);
        } catch (NumberFormatException e) {
            callback.onIncorrectMessage(message);
            return;
        }

        checkTime(time);

        lat = toDegrees(lat, splitMessage[4].equals("N"));
        lon = toDegrees(lon, splitMessage[6].equals("E"));

        location.setTime(time);
        location.setLatitude(lat);
        location.setLongitude(lon);

        speed = speed * KNOTS_TO_MS;

        if (hasSpeed)
            location.setSpeed(speed);

        if (hasBearing)
            location.setBearing(bearing);

        parsedMessages.rmc = true;
    }

    private void parseZDA(String message) {
        /*
            Message format:
             1) Time (UTC)
             2) Day
             3) Month
             4) Year
             5) Local timezone offset - hours
             6) Local timezone offset - minutes
             7) Checksum
         */

        String[] splitMessage = message.split(",");
        splitMessage[splitMessage.length-1] = splitMessage[splitMessage.length-1].split("[*]")[0];

        if (splitMessage.length != 7) {
            callback.onIncorrectMessage(message);
            return;
        }

        long time;
        int day;
        int month;
        int year;

        try {
            time = Long.parseLong(splitMessage[1].substring(0, splitMessage[1].indexOf('.')));
            day = Integer.parseInt(splitMessage[2]);
            month = Integer.parseInt(splitMessage[3]);
            year = Integer.parseInt(splitMessage[4]);
        } catch (NumberFormatException e) {
            callback.onIncorrectMessage(message);
            return;
        }

        checkTime(time);

        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        c.set(year, month - 1, day, 0, 0, 0);

        currentDate = c.getTimeInMillis() / 1000;

        parsedMessages.zda = true;
    }
}
