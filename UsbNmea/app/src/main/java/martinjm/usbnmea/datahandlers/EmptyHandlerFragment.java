package martinjm.usbnmea.datahandlers;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import martinjm.usbnmea.DataHandlerSettings;
import martinjm.usbnmea.R;
import martinjm.usbnmea.UsbConnection.UsbStatus;

public class EmptyHandlerFragment extends DataHandlerSettings {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_empty_handler, container, false);
    }

    public void updateStatus(UsbStatus usbStatus, Object dataHandlerStatus) {
        // There is nothing to do.
    }
}
