package martinjm.usbnmea.datahandlers;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class SkyplotView extends View {

    private Paint backgroundPaint;
    private Paint satellitePaint;

    private final int activeSatelliteColor = Color.argb(0xff, 0x00, 0xff, 0x00);
    private final int nonActiveSatelliteColor = Color.argb(0xff, 0xff, 0x00, 0x00);

    private NmeaParser.Satellite[] viewedSatellites;
    private Integer[] activeSatellites;

    public SkyplotView(Context context) {
        super(context);
        init();
    }

    public SkyplotView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SkyplotView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        backgroundPaint = new Paint();
        backgroundPaint.setARGB(0xff, 0xab, 0xab, 0xab);
        backgroundPaint.setStrokeWidth(1.0f);
        backgroundPaint.setStyle(Paint.Style.STROKE);
        backgroundPaint.setAntiAlias(true);

        satellitePaint = new Paint();
        satellitePaint.setStyle(Paint.Style.FILL);
        satellitePaint.setAntiAlias(true);
        satellitePaint.setStrokeWidth(1.5f);
    }

    public void setSatellites(NmeaParser.Satellite[] viewedSatellites, Integer[] activeSatellites) {
        this.viewedSatellites = viewedSatellites;
        this.activeSatellites = activeSatellites;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawBackground(canvas);

        if (viewedSatellites != null) {
            for (NmeaParser.Satellite satellite : viewedSatellites) {
                if (satellite != null)
                    drawSatellite(canvas, satellite);
            }
        }
    }

    private int[] calculateLocation(int elevation, int azimuth) {
        float size = getWidth();
        float halfSize = size / 2;

        float scale = halfSize / 180;
        float distance = (90 - elevation) * scale;

        double rad = Math.toRadians(azimuth - 90);
        int x = (int) (distance * Math.cos(rad) + halfSize);
        int y = (int) (distance * Math.sin(rad) + halfSize);

        return new int[] {x, y};
    }

    private void drawBackground(Canvas canvas) {
        // The view is square, which we rely on here
        float size = getWidth() ;
        float halfSize = size / 2;
        float quarterSize = size / 4;

        canvas.drawLine(halfSize, 0, halfSize, size, backgroundPaint);
        canvas.drawLine(0, halfSize, size, halfSize, backgroundPaint);

        // The minus one on the radius is to prevent flat spots
        canvas.drawCircle(halfSize, halfSize, halfSize-1, backgroundPaint);
        canvas.drawCircle(halfSize, halfSize, quarterSize, backgroundPaint);
    }

    private void drawSatellite(Canvas canvas, NmeaParser.Satellite satellite) {
        int[] location = calculateLocation(satellite.elevationDeg, satellite.azimuthDeg);

        satellitePaint.setColor(nonActiveSatelliteColor);
        for (Integer id : activeSatellites) {
            if (id == satellite.idNum) {
                satellitePaint.setColor(activeSatelliteColor);
                break;
            }
        }

        switch (satellite.constellation) {
            case NmeaParser.Constellation.GPS:
                drawGpsSatellite(canvas, location, satellitePaint);
                break;
            case NmeaParser.Constellation.GLONASS:
                drawGlonassSatellite(canvas, location, satellitePaint);
                break;
            case NmeaParser.Constellation.GALILEO:
                drawGalileoSatellite(canvas, location, satellitePaint);
                break;
            case NmeaParser.Constellation.BEIDOU:
                drawBeidouSatellite(canvas, location, satellitePaint);
                break;
            default:
                drawAnySatellite(canvas, location, satellitePaint);
                break;
        }
    }

    private void drawGpsSatellite(Canvas canvas, int[] location, Paint paint) {
        canvas.drawCircle(location[0], location[1], 4f, paint);
    }

    private void drawGlonassSatellite(Canvas canvas, int[] location, Paint paint) {
        canvas.drawRect(new Rect(location[0] - 4, location[1] - 4, location[0] + 4, location[1] + 4), paint);
    }

    private void drawGalileoSatellite(Canvas canvas, int[] location, Paint paint) {
        float size = 4.5f;

        Path rhombus = new Path();
        rhombus.moveTo(location[0], location[1] - size);
        rhombus.lineTo(location[0] + size, location[1]);
        rhombus.lineTo(location[0], location[1] + size);
        rhombus.lineTo(location[0] - size, location[1]);
        rhombus.close();

        canvas.drawPath(rhombus, paint);
    }

    private void drawBeidouSatellite(Canvas canvas, int[] location, Paint paint) {
        Path triangle = new Path();
        triangle.moveTo(location[0], location[1] - 5.3f);
        triangle.lineTo(location[0] + 4.6f, location[1] + 2.7f);
        triangle.lineTo(location[0] - 4.6f, location[1] + 2.7f);
        triangle.close();

        canvas.drawPath(triangle, paint);
    }

    private void drawAnySatellite(Canvas canvas, int[] location, Paint paint) {
        float size = 3.5f;

        canvas.drawLine(location[0] - size, location[1] - size, location[0] + size, location[1] + size, paint);
        canvas.drawLine(location[0] - size, location[1] + size, location[0] + size, location[1] - size, paint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        // Make the view square (bounded on width)
        int width = getMeasuredWidth();
        //noinspection SuspiciousNameCombination
        setMeasuredDimension(width, width);
    }
}
