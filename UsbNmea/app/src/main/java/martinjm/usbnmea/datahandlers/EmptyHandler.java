package martinjm.usbnmea.datahandlers;

import android.content.Intent;

import martinjm.usbnmea.DataHandler;

public class EmptyHandler extends DataHandler {

    @Override
    public void handleIntent(Intent intent) {}

    @Override
    public void handleData(byte[] data) {}
}
