package martinjm.usbnmea.datahandlers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import martinjm.usbnmea.DataHandlerSettings;
import martinjm.usbnmea.MainActivity;
import martinjm.usbnmea.R;
import martinjm.usbnmea.UsbConnection.UsbStatus;

public class LoggerFragment extends DataHandlerSettings {

    private final SimpleDateFormat fileDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_SS", Locale.getDefault());

    private static final int CHANGE_OUTPUT_REQUEST_CODE = 132;

    private final BroadcastReceiver loggerIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == null)
                return;

            switch (intent.getAction()) {
                case Logger.REPLIES.STATUS_CHANGE:
                case Logger.REPLIES.ERROR:
                    MainActivity main = (MainActivity) getActivity();
                    if (main != null)
                        main.updateStatus();
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_logger, container, false);

        v.findViewById(R.id.btnChangeOutput).setOnClickListener(this::onChangeOutputClick);
        v.findViewById(R.id.btnStartLogging).setOnClickListener(this::onStartLoggingClick);
        v.findViewById(R.id.btnStopLogging).setOnClickListener(this::onStopLoggingClick);

        IntentFilter filter = new IntentFilter();
        filter.addAction(Logger.REPLIES.STATUS_CHANGE);
        filter.addAction(Logger.REPLIES.ERROR);

        MainActivity main = (MainActivity) getActivity();
        if (main != null)
            main.registerReceiver(loggerIntentReceiver, filter);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        MainActivity main = (MainActivity) getActivity();
        if (main != null)
            main.unregisterReceiver(loggerIntentReceiver);
    }

    public void updateStatus(UsbStatus usbStatus, Object dataHandlerStatus) {
        int logStatusStringId = R.string.LogStatusNotLogging;
        String filePath = "";

        boolean btnChangeOutputEnabled = false;
        boolean btnStartLoggingEnabled = false;
        boolean btnStopLoggingEnabled = false;

        if (usbStatus != UsbStatus.Disconnected) {
            if (dataHandlerStatus == null) {
                // TODO: handle properly?
                Log.w("UsbNmea", "Data handler status is null");
                return;
            }

            if (!Logger.Status.class.isAssignableFrom(dataHandlerStatus.getClass())) {
                // TODO: handle properly?
                Log.w("UsbNmea", "Data handler status is not of type logger status");
                return;
            }

            Logger.Status logStatus = (Logger.Status) dataHandlerStatus;

            if (logStatus.logStatus == Logger.LogStatus.Logging) {
                // btnChangeOutputEnabled = false;
                // btnStartLoggingEnabled = false;
                btnStopLoggingEnabled = true;

                logStatusStringId = R.string.LogStatusLogging;
            } else {
                btnChangeOutputEnabled = true;
                btnStartLoggingEnabled = true;
                // btnStopLoggingEnabled = false;
            }

            Log.d("usbNmeaDebugging", "Update file uri: " + logStatus.fileUri);

            if (logStatus.fileUri == null && getActivity() != null) {
                // Set a filename suggestion
                String filename = "log_" + fileDateFormat.format(Calendar.getInstance().getTime()) + ".nmea";
                File parentPath = getActivity().getExternalFilesDir(null);
                File outputFile = new File(parentPath, filename);
                filePath = outputFile.getPath();

                Log.d("usbNmeaDebugging", "Update filepath null, setting to: " + filePath);

                // Set filepath in the data handler
                Logger.Status status = new Logger.Status();
                status.logStatus = null;
                status.fileUri = Uri.fromFile(outputFile);
                ((MainActivity) getActivity()).changeDataHandlerStatus(status);
            } else {
                filePath = logStatus.fileUri.getPath();
            }
        }

        MainActivity main = (MainActivity) getActivity();
        if (main != null) {
            ((TextView) main.findViewById(R.id.tvLogStatusDynamic)).setText(logStatusStringId);
            ((TextView) main.findViewById(R.id.tvOutputDynamic)).setText(filePath);

            main.findViewById(R.id.btnChangeOutput).setEnabled(btnChangeOutputEnabled);
            main.findViewById(R.id.btnStartLogging).setEnabled(btnStartLoggingEnabled);
            main.findViewById(R.id.btnStopLogging).setEnabled(btnStopLoggingEnabled);
        }
    }

    // TODO: change when android docs are updated:
    //       https://developer.android.com/training/data-storage/shared/documents-files#create-file
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CHANGE_OUTPUT_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            Uri fileUri = data.getData();

            Log.d("usbNmeaDebugging", "Setting file uri to: " + fileUri);

            // Set filepath in data handler
            Logger.Status status = new Logger.Status();
            status.logStatus = null;
            status.fileUri = fileUri;
            MainActivity main = (MainActivity) getActivity();
            if (main != null) {
                main.changeDataHandlerStatus(status);
                if (fileUri != null)
                    ((TextView) main.findViewById(R.id.tvOutputDynamic)).setText(fileUri.getPath());
            }
        }
    }

    private void onChangeOutputClick(View v) {
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/*");
        intent.putExtra(Intent.EXTRA_TITLE, "log_" + fileDateFormat.format(Calendar.getInstance().getTime()) + ".nmea");
        startActivityForResult(intent, CHANGE_OUTPUT_REQUEST_CODE);
    }

    private void onStartLoggingClick(View v) {
        Logger.Status status = new Logger.Status();
        status.logStatus = Logger.LogStatus.Logging;
        status.fileUri = null;

        MainActivity main = (MainActivity) getActivity();
        if (main != null)
            main.changeDataHandlerStatus(status);
    }

    private void onStopLoggingClick(View v) {
        Logger.Status status = new Logger.Status();
        status.logStatus = Logger.LogStatus.NotLogging;
        status.fileUri = null;

        MainActivity main = (MainActivity) getActivity();
        if (main != null)
            main.changeDataHandlerStatus(status);
    }
}
