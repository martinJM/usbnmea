package martinjm.usbnmea.datahandlers;

import android.location.Location;
import android.os.Build;
import android.util.Log;

import org.junit.Test;

import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/*
 * Some nice resources to create test data:
 *  - Create data independent of your own location:
 *      https://www.nmeagen.org/
 *      The precision in the lat/lon is slightly lower than my antenna
 *  - Create a valid checksum over any message:
 *      https://nmeachecksum.eqth.net/
 */

@RunWith(MockitoJUnitRunner.class)
public class NmeaParserTest {

    @Mock
    private NmeaParser.NmeaParserCallback npc;

    @Captor
    private ArgumentCaptor<Location> locationArgumentCaptor;

    @Test
    public void testZeroGnLocation() throws InterruptedException {
        String gga_message = "$GNGGA,0.0,0,N,0,E,1,00,0,0,M,0,M, , *6C";
        String rmc_message = "$GNRMC,0.0,A,0,N,0,E,0,0, , , *3D";

        NmeaParser np = new NmeaParser(npc, "test_provider");
        np.parseMessage(gga_message);
        np.parseMessage(rmc_message);

        Thread.sleep(20);

        verify(npc).onLocation(
                eq(new NmeaParser.ParsedMessages(true, true, false)),
                locationArgumentCaptor.capture()
        );

        Location location = locationArgumentCaptor.getValue();

        assertEquals(0, location.getLatitude(), 0.0000001);
        assertEquals(0, location.getLongitude(), 0.0000001);
        assertEquals(0, location.getExtras().getInt("satellites"));
        assertEquals(0, location.getAccuracy(), 0.001);
        assertEquals(0, location.getAltitude(), 0.001);
        assertEquals(0, location.getSpeed(), 0.001);
        assertEquals(0, location.getBearing(), 0.001);
    }

    @Test
    public void testZeroGpLocation() throws InterruptedException {
        String gga_message = "$GPGGA,0.0,0,N,0,E,1,00,0,0,M,0,M, , *72";
        String rmc_message = "$GPRMC,0.0,A,0,N,0,E,0,0, , , *23";

        NmeaParser np = new NmeaParser(npc, "test_provider");
        np.parseMessage(rmc_message);
        np.parseMessage(gga_message);

        Thread.sleep(20);

        verify(npc).onLocation(
                eq(new NmeaParser.ParsedMessages(true, true, false)),
                locationArgumentCaptor.capture()
        );

        Location location = locationArgumentCaptor.getValue();

        assertEquals(0, location.getLatitude(), 0.0000001);
        assertEquals(0, location.getLongitude(), 0.0000001);
        assertEquals(0, location.getExtras().getInt("satellites"));
        assertEquals(0, location.getAccuracy(), 0.001);
        assertEquals(0, location.getAltitude(), 0.001);
        assertEquals(0, location.getSpeed(), 0.001);
        assertEquals(0, location.getBearing(), 0.001);
    }

    @Test
    public void testBerlinLocation() throws InterruptedException {
        String gga_message = "$GPGGA,0.0,5231.099,N,01321.758,E,1,12,1.0,0.0,M,0.0,M,,*50";
        String rmc_message = "$GPRMC,0.0,A,5231.099,N,01321.758,E,,,,000.0,W*44";

        NmeaParser np = new NmeaParser(npc, "test_provider");
        np.parseMessage(gga_message);
        np.parseMessage(rmc_message);

        Thread.sleep(20);

        verify(npc).onLocation(
                eq(new NmeaParser.ParsedMessages(true, true, false)),
                locationArgumentCaptor.capture()
        );

        Location location = locationArgumentCaptor.getValue();

        assertEquals(52.51831, location.getLatitude(), 0.00001);
        assertEquals(13.36263, location.getLongitude(), 0.00001);
        assertEquals(12, location.getExtras().getInt("satellites"));
        assertEquals(4.7f, location.getAccuracy(), 0.001);
        assertEquals(0, location.getAltitude(), 0.001);
        assertFalse(location.hasSpeed());
        assertFalse(location.hasBearing());
    }

    @Test
    public void testPanamaLocation() throws InterruptedException {
        String gga_message = "$GPGGA,191328.432,3326.608,S,07039.473,W,1,12,1.0,0.0,M,0.0,M,,*63";
        String rmc_message = "$GPRMC,191328.432,A,3326.608,S,07039.473,W,,,,000.0,W*77";

        NmeaParser np = new NmeaParser(npc, "test_provider");
        np.parseMessage(gga_message);
        np.parseMessage(rmc_message);

        Thread.sleep(20);

        verify(npc).onLocation(
                eq(new NmeaParser.ParsedMessages(true, true, false)),
                locationArgumentCaptor.capture()
        );

        Location location = locationArgumentCaptor.getValue();

        assertEquals(-33.44347, location.getLatitude(), 0.00001);
        assertEquals(-70.65788, location.getLongitude(), 0.00001);
        assertEquals(12, location.getExtras().getInt("satellites"));
        assertEquals(4.7f, location.getAccuracy(), 0.001);
        assertEquals(0, location.getAltitude(), 0.001);
        assertFalse(location.hasSpeed());
        assertFalse(location.hasBearing());
    }

    @Test
    public void testEpochZdaMessage() throws NoSuchFieldException, IllegalAccessException {
        String zda_message = "$GNZDA,0.0,01,01,1970,,*77";

        NmeaParser np = new NmeaParser(npc, "test_provider");
        np.parseMessage(zda_message);

        Field privateCurrentDate = NmeaParser.class.getDeclaredField("currentDate");
        privateCurrentDate.setAccessible(true);

        long currentDate = (long) privateCurrentDate.get(np);

        assertEquals(0, currentDate);
    }

    @Test
    public void testZdaMessage() throws NoSuchFieldException, IllegalAccessException {
        String zda_message = "$GNZDA,100957.000,17,04,2021,,*41";

        NmeaParser np = new NmeaParser(npc, "test_provider");
        np.parseMessage(zda_message);

        Field privateCurrentDate = NmeaParser.class.getDeclaredField("currentDate");
        privateCurrentDate.setAccessible(true);

        long currentDate = (long) privateCurrentDate.get(np);

        assertEquals(1618617600, currentDate);
    }

    @Test
    public void testZdaPlusZeroLocationTime() throws InterruptedException {
        String zda_message = "$GNZDA,100957.000,17,04,2021,,*41";
        String gga_message = "$GNGGA,0.0,0,N,0,E,1,00,0,0,M,0,M, , *6C";
        String rmc_message = "$GNRMC,0.0,A,0,N,0,E,0,0, , , *3D";

        NmeaParser np = new NmeaParser(npc, "test_provider");
        np.parseMessage(zda_message);
        np.parseMessage(gga_message);
        np.parseMessage(rmc_message);

        Thread.sleep(20);

        verify(npc).onLocation(
                eq(new NmeaParser.ParsedMessages(true, true, true)),
                locationArgumentCaptor.capture()
        );

        Location location = locationArgumentCaptor.getValue();

        assertEquals(1618617600, location.getTime());
    }

    @Test
    public void testZdaPlusNonzeroLocationTime() throws InterruptedException {
        String zda_message = "$GNZDA,100957.000,17,04,2021,,*41";
        String gga_message = "$GNGGA,10.0,0,N,0,E,1,00,0,0,M,0,M, , *5D";
        String rmc_message = "$GNRMC,10.0,A,0,N,0,E,0,0, , , *0C";

        NmeaParser np = new NmeaParser(npc, "test_provider");
        np.parseMessage(zda_message);
        np.parseMessage(gga_message);
        np.parseMessage(rmc_message);

        Thread.sleep(20);

        verify(npc).onLocation(
                eq(new NmeaParser.ParsedMessages(true, true, true)),
                locationArgumentCaptor.capture()
        );

        Location location = locationArgumentCaptor.getValue();

        assertEquals(1618617610, location.getTime());
    }

    @Test
    public void testIncorrectMessage() {
        // This tests a random message (you can get these when the baudrate is incorrect)
        String message = "e4nlg6lN0rBFLe6DAMhZ";

        NmeaParser np = new NmeaParser(npc, "test_provider");
        np.parseMessage(message);

        verify(npc).onIncorrectMessage(message);
    }

    @Test
    @SuppressWarnings({"unchecked", "ConstantConditions"})
    public void testGsaNoFix() throws NoSuchFieldException, IllegalAccessException {
        String gps_message = "$GPGSA,A,1,,,,,,,,,,,,,,,*1E";
        String glonass_message = "$GLGSA,A,1,,,,,,,,,,,,,,,*02";
        String galileo_message = "$GAGSA,A,1,,,,,,,,,,,,,,,*0F";
        String beidou_message = "$GBGSA,A,1,,,,,,,,,,,,,,,*0C";

        NmeaParser np = new NmeaParser(npc, "test_provider");

        Field privateActiveSatellites = NmeaParser.class.getDeclaredField("activeSatellites");
        privateActiveSatellites.setAccessible(true);

        np.parseMessage(gps_message);
        assertEquals(0, ((List<Integer>) privateActiveSatellites.get(np)).size());

        np.parseMessage(glonass_message);
        assertEquals(0, ((List<Integer>) privateActiveSatellites.get(np)).size());

        np.parseMessage(galileo_message);
        assertEquals(0, ((List<Integer>) privateActiveSatellites.get(np)).size());

        np.parseMessage(beidou_message);
        assertEquals(0, ((List<Integer>) privateActiveSatellites.get(np)).size());

        verify(npc, never()).onIncorrectMessage(anyString());
    }

    @Test
    @SuppressWarnings({"unchecked", "ConstantConditions"})
    public void testGsa2dFix() throws NoSuchFieldException, IllegalAccessException {
        String gps_message = "$GPGSA,A,2,02,,,,,,,,,,,,1.00,1.00,1.00*00";
        String glonass_message = "$GLGSA,A,2,03,,,,,,,,,,,,1.00,1.00,1.00*1D";
        String galileo_message = "$GAGSA,A,2,04,,,,,,,,,,,,1.00,1.00,1.00*17";
        String beidou_message = "$GBGSA,A,2,05,,,,,,,,,,,,1.00,1.00,1.00*15";

        NmeaParser np = new NmeaParser(npc, "test_provider");

        np.parseMessage(gps_message);
        np.parseMessage(glonass_message);
        np.parseMessage(galileo_message);
        np.parseMessage(beidou_message);

        verify(npc, never()).onIncorrectMessage(anyString());

        Field privateActiveSatellites = NmeaParser.class.getDeclaredField("activeSatellites");
        privateActiveSatellites.setAccessible(true);

        Integer[] activeSatellites = ((List<Integer>) privateActiveSatellites.get(np)).toArray(new Integer[0]);

        assertArrayEquals(
                new Integer[]{2, 3, 4, 5},
                activeSatellites
        );

        Field privateLocation = NmeaParser.class.getDeclaredField("location");
        privateLocation.setAccessible(true);

        Location location = (Location) privateLocation.get(np);

        assertNotNull(location);
        assertEquals(4.7f, location.getAccuracy(), 0.001);
    }

    @Test
    @SuppressWarnings({"unchecked", "ConstantConditions"})
    public void testGsa3dFix() throws NoSuchFieldException, IllegalAccessException {
        String gps_message = "$GPGSA,A,3,02,06,,,,,,,,,,,1.00,1.00,1.00*07";
        String glonass_message = "$GLGSA,A,3,03,07,,,,,,,,,,,1.00,1.00,1.00*1B";
        String galileo_message = "$GAGSA,A,3,04,08,,,,,,,,,,,1.00,1.00,1.00*1E";
        String beidou_message = "$GBGSA,A,3,05,09,,,,,,,,,,,1.00,1.00,1.00*1D";

        NmeaParser np = new NmeaParser(npc, "test_provider");

        np.parseMessage(gps_message);
        np.parseMessage(glonass_message);
        np.parseMessage(galileo_message);
        np.parseMessage(beidou_message);

        verify(npc, never()).onIncorrectMessage(anyString());

        Field privateActiveSatellites = NmeaParser.class.getDeclaredField("activeSatellites");
        privateActiveSatellites.setAccessible(true);

        Integer[] activeSatellites = ((List<Integer>) privateActiveSatellites.get(np)).toArray(new Integer[0]);

        assertArrayEquals(
                new Integer[]{2, 6, 3, 7, 4, 8, 5, 9},
                activeSatellites
        );

        Field privateLocation = NmeaParser.class.getDeclaredField("location");
        privateLocation.setAccessible(true);

        Location location = (Location) privateLocation.get(np);

        assertNotNull(location);
        assertEquals(4.7f, location.getAccuracy(), 0.001);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            assertEquals(4.7f, location.getVerticalAccuracyMeters(), 0.001);
        else
            Log.w("UsbNmeaTesting", "Vertical accuracy is not supported on this device.");
    }

    @Test
    @SuppressWarnings({"unchecked", "ConstantConditions"})
    public void testGsvNoSats() throws NoSuchFieldException, IllegalAccessException {
        String gps_message = "$GPGSV,1,1,00*79";
        String glonass_message = "$GLGSV,1,1,00*65";
        String galileo_message = "$GAGSV,1,1,00*68";
        String beidou_message = "$GBGSV,1,1,00*6B";

        NmeaParser np = new NmeaParser(npc, "test_provider");

        np.parseMessage(gps_message);
        np.parseMessage(glonass_message);
        np.parseMessage(galileo_message);
        np.parseMessage(beidou_message);

        verify(npc, never()).onIncorrectMessage(anyString());

        Field privateViewedSatellites = NmeaParser.class.getDeclaredField("viewedSatellites");
        privateViewedSatellites.setAccessible(true);

        assertEquals(0, ((List<NmeaParser.Satellite>) privateViewedSatellites.get(np)).size());
    }

    @Test
    @SuppressWarnings({"unchecked", "ConstantConditions"})
    public void testGsvSingleMessages() throws NoSuchFieldException, IllegalAccessException {
        String gps_message = "$GPGSV,1,1,01,01,01,01,01*78";
        String glonass_message = "$GLGSV,1,1,02,02,02,02,02,03,03,03,03*67";
        String galileo_message = "$GAGSV,1,1,03,04,04,04,04,05,05,05,05,06,06,06,06*6B";
        String beidou_message = "$GBGSV,1,1,04,07,07,07,07,08,08,08,08,09,09,09,09,10,10,10,10*6F";

        NmeaParser np = new NmeaParser(npc, "test_provider");

        np.parseMessage(gps_message);
        np.parseMessage(glonass_message);
        np.parseMessage(galileo_message);
        np.parseMessage(beidou_message);

        verify(npc, never()).onIncorrectMessage(anyString());

        Field privateViewedSatellites = NmeaParser.class.getDeclaredField("viewedSatellites");
        privateViewedSatellites.setAccessible(true);

        NmeaParser.Satellite[] satellites = ((List<NmeaParser.Satellite>) privateViewedSatellites.get(np)).toArray(new NmeaParser.Satellite[0]);

        assertArrayEquals(
                new NmeaParser.Satellite[] {
                        new NmeaParser.Satellite(NmeaParser.Constellation.GPS, 1, 1, 1, 1),
                        new NmeaParser.Satellite(NmeaParser.Constellation.GLONASS, 2, 2, 2, 2),
                        new NmeaParser.Satellite(NmeaParser.Constellation.GLONASS, 3, 3, 3, 3),
                        new NmeaParser.Satellite(NmeaParser.Constellation.GALILEO, 4, 4, 4, 4),
                        new NmeaParser.Satellite(NmeaParser.Constellation.GALILEO, 5, 5, 5, 5),
                        new NmeaParser.Satellite(NmeaParser.Constellation.GALILEO, 6, 6, 6, 6),
                        new NmeaParser.Satellite(NmeaParser.Constellation.BEIDOU, 7, 7, 7, 7),
                        new NmeaParser.Satellite(NmeaParser.Constellation.BEIDOU, 8, 8, 8, 8),
                        new NmeaParser.Satellite(NmeaParser.Constellation.BEIDOU, 9, 9, 9, 9),
                        new NmeaParser.Satellite(NmeaParser.Constellation.BEIDOU, 10, 10, 10, 10)
                },
                satellites
        );
    }

    @Test
    @SuppressWarnings({"unchecked", "ConstantConditions"})
    public void testGsvMultipleMessages() throws NoSuchFieldException, IllegalAccessException {
        String gps_message1 = "$GPGSV,2,1,05,01,01,01,01,02,02,02,02,03,03,03,03,04,04,04,04*7F";
        String gps_message2 = "$GPGSV,2,2,05,05,05,05,05*7C";

        NmeaParser np = new NmeaParser(npc, "test_provider");

        np.parseMessage(gps_message1);
        np.parseMessage(gps_message2);

        verify(npc, never()).onIncorrectMessage(anyString());

        Field privateViewedSatellites = NmeaParser.class.getDeclaredField("viewedSatellites");
        privateViewedSatellites.setAccessible(true);

        NmeaParser.Satellite[] satellites = ((List<NmeaParser.Satellite>) privateViewedSatellites.get(np)).toArray(new NmeaParser.Satellite[0]);

        assertArrayEquals(
                new NmeaParser.Satellite[] {
                        new NmeaParser.Satellite(NmeaParser.Constellation.GPS, 1, 1, 1, 1),
                        new NmeaParser.Satellite(NmeaParser.Constellation.GPS, 2, 2, 2, 2),
                        new NmeaParser.Satellite(NmeaParser.Constellation.GPS, 3, 3, 3, 3),
                        new NmeaParser.Satellite(NmeaParser.Constellation.GPS, 4, 4, 4, 4),
                        new NmeaParser.Satellite(NmeaParser.Constellation.GPS, 5, 5, 5, 5)
                },
                satellites
        );
    }

    // TODO: write test for missing values in GSV messages

    @Test
    public void testGstAccuracyMessage() throws NoSuchFieldException, IllegalAccessException {
        String gst_message = "$GPGST,0.0,0.0,0.0,0.0,0.0,1.0,1.0,1.0*56";
        String gga_message = "$GNGGA,0.0,0.0,N,0.0,E,1,00,1.00,0.0,M,0.0,M,,*43";

        NmeaParser np = new NmeaParser(npc, "test_provider");

        np.parseMessage(gst_message);
        np.parseMessage(gga_message);

        verify(npc, never()).onIncorrectMessage(anyString());

        Field privateLocation = NmeaParser.class.getDeclaredField("location");
        privateLocation.setAccessible(true);

        Location location = (Location) privateLocation.get(np);

        assertNotNull(location);
        assertEquals(1.0f, location.getAccuracy(), 0.001);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            assertEquals(1.0f, location.getVerticalAccuracyMeters(), 0.001);
        else
            Log.w("UsbNmeaTesting", "Vertical accuracy is not supported on this device.");
    }
}
