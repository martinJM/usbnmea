# UsbNmea

## What is it?
UsbNmea is an Android application that aims to offer some support to USB GNSS antenna's on Android.

It supports different data handlers as to support different use cases.
Currently there are four data handlers:
 - None  
   This does nothing with the data it gets
 - File Logger  
   This saves the data it gets from the antenna into a file on your device.
   To convert this file to a GPX file, I recommend using [GpsBabel](https://www.gpsbabel.org/) on your computer.
 - Mock location  
   This injects the data from the external GNSS antenna into Android, so it can be used by other apps that support GPS.
   Note that the accuracy circle some apps produce may not be correct.
   It's also not possible to inject information about satellites, so skyplots in other apps won't work.
 - Skyplot  
   This shows a skyplot with the satellites that are currently in view.
   They are colored green if they are used for the fix, and red otherwise.
   The shape shows the type of satellite (circle for GPS, square for Glonass, rhombus (diamond) for Galileo, triangle for Beidou, and a cross for anything else).

## Why?
I bought an USB GNSS antenna and wanted to use it on my Android phone.

## How?
Build the app using [Android Studio](https://developer.android.com/studio/install) or download one of the [releases](https://gitlab.com/martinJM/usbnmea/-/releases).
Then install it on your phone, connect an USB GNSS antenna to your phone using an OTG cable and press the buttons in the app.

I'm building on Linux myself, so if there are issues on other platforms I may not be able to help.

## No, I meant: how does it work?
It starts a service that should stay on in the background, which gets the data from the USB antenna and hands it off to the selected data handler.

To get the data from the USB antenna, I use the UsbSerial library from Felipe Herranz, [available on GitHub](https://github.com/felHR85/UsbSerial).
Since his code is licensed under the MIT license, I decided to do the same for my code.
If you like that, you can support him through the options he lists in the README of the UsbSerial library.

# Users beware!
I have tested the application manually a bit, not always with the best results.
This is mostly because the micro-usb connection of my phone is worn out quite a bit, and the antenna wiggles itself out.
Therefore, I cannot make any guarantees as to how well the app will hold up for longer periods.
I would suggest only using it as a secondary source of location.

If you try this app, please create a pull-request for the [stability](Stability.md) document, so everyone can see how well the app seems to behave.
If the app didn't perform well, this also gives the opportunity to discuss why it did not.
