# Stability of the application

## How to add data
Please make a pull request, where you have prepended the following block to the Data section below:
```
App version: [Put the version of the app you've used here]
<br>
Android version: [Put the Android version you've used here]
<br>
Device: [Put your device info here]
<br>
Stability: [Good/Bad/some other short description]
<br>
Duration: [Duration of the log]
<br>
Logging interval: [The interval you were logging at]
<br>
Info:<br>
[More information on the stability]
___

```

Note that I'm not asking for the logfile itself, as this discloses where you are at what time. If you think there is information in the file that is vital, you can create a private issue through the [issue tracker](https://gitlab.com/martinJM/usbnmea/-/issues). Make sure to tick the box that the issue is confidential!

If you have questions about what to do/how to do it/where to find some of the information, please also create an issue in the [issue tracker](https://gitlab.com/martinJM/usbnmea/-/issues). It'll give me the opportunity to improve this document.

## Data

App version: v0.1.0
<br>
Android version: Android 5.0.1
<br>
Device: Lenovo Tab3 7 Essential
<br>
Stability: Good
<br>
Duration: 8 hours
<br>
Logging interval: 1 second
<br>
Info:<br>
Both the tablet and antenna were stationary with good GPS reception. A quick script proved that it saved a fix every second of those 8 hours. After using `gpsbabel` to convert the data to a GPX, the location wasn't off more than 40m at any given time. Most of the data was much closer. `gpxinfo` reports that the average distance between the points was 0.14m, the total length in 3D was 7.996km (2D: 4.066km), and the max speed was 0.55m/s = 1.97km/h.
___
